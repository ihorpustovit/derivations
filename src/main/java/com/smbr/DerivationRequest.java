package com.smbr;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.function.Function;

@AllArgsConstructor
@Getter
@Setter
public class DerivationRequest {
    private Function<Double,Double> functionToDerive;
    private Double upperBound;
    private Double lowerBound;
    private Double delta;


}
