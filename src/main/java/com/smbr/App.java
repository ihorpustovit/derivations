package com.smbr;

import java.util.concurrent.*;
import java.util.function.Function;

public class App {
    public static void main(String[] args) {
        Function<Double,Double> function = x -> (1.0 / 12.0) * Math.pow(x,4) + (1.0 / 3.0) * x - (1.0 / 60.0);
        Function<Double,Double> function2 = x -> Math.sqrt(1 + Math.log(x));
        TrapeziumDerivationService derivationService = new TrapeziumDerivationService();
        DerivationRequest derivationRequest = new DerivationRequest(function2,3.0,1.0, Math.pow(10,-6));
        System.out.println("Результат інтегрування функції x^x * (1 + ln(x)) на відрізку [1, 3]: "+derivationService.doDerive(derivationRequest));
    }
}
