package com.smbr;

public class TrapeziumDerivationService implements DerivationService{


    @Override
    public Double doDerive(DerivationRequest derivationRequest){
        int n = 5;
        Double forward;
        Double backward;
        int iteration = 0;

        do{
            backward = derive(derivationRequest, n);
            forward = derive(derivationRequest, n * 2);
            n *= 2;
            iteration++;
            System.out.println("Calculated error on iteration "+iteration+": "+calculateError(forward,backward));
        }while (calculateError(forward,backward) >= derivationRequest.getDelta());

        return forward;
    }

    public Double derive(DerivationRequest derivationRequest, Integer n){
        var lowerBound = derivationRequest.getLowerBound();
        var upperBound = derivationRequest.getUpperBound();
        var function = derivationRequest.getFunctionToDerive();

        Double h = calculateIterationStep(derivationRequest,n);
        Double sum = 0.0;

        for (double i = lowerBound + h; i < upperBound; i+=h){
            sum += function.apply(i);
        }

        sum += (function.apply(lowerBound) + function.apply(upperBound)) / 2;

        return sum * h;
    }

    private Double calculateIterationStep(DerivationRequest derivationRequest,Integer n){
        return (derivationRequest.getUpperBound() - derivationRequest.getLowerBound()) / n;
    }

    private Double calculateError(Double forwardDerivationResult, Double backwardDerivationResult){
        return Math.abs((backwardDerivationResult - forwardDerivationResult) / 3);
    }
}
