package com.smbr;

public interface DerivationService {
    Double doDerive(DerivationRequest derivationRequest);
}
